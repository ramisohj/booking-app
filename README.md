# booking-app

## Getting Started

### Prerequisites

- Java SDK 17.0+
- Maven 3.9+
- Docker
- Node.js 18.16+

---
## Services
### database
-  PORT = `5432`
### zookeeper
-  PORT = `2181`
### kafka
-  PORT = `29092`
### registration-service
-  PORT = `2000`
### search-service
-  PORT = `2002`
### notification-service
-  PORT = `2004`


## How to run the the project

In order to run the application just run the next command:

```
sudo bash booking-app.sh 
```

## More Info

* Dropwizard: https://www.dropwizard.io/en/stable/
* Dropwizard JDBI3: https://www.dropwizard.io/en/stable/manual/jdbi3.html