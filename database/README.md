# database

Service information
---

SERVICE_NAME: database  
PORT: 5432

How to run database service in docker
---

1. Run `docker build -t  postgres .`
1. Run `docker run -p 5432:5432 -it postgres`

How to run database in docker using dockercompose
---

1. Run `docker-compose up`