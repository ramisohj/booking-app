PGDMP     (    0                {            postgres    15.4 (Debian 15.4-1.pgdg120+1)    15.3 !    A           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            B           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            C           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            D           1262    5    postgres    DATABASE     s   CREATE DATABASE postgres WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE_PROVIDER = libc LOCALE = 'en_US.utf8';
    DROP DATABASE postgres;
                postgres    false            E           0    0    DATABASE postgres    COMMENT     N   COMMENT ON DATABASE postgres IS 'default administrative connection database';
                   postgres    false    3396            �            1259    16384    cities    TABLE     z   CREATE TABLE public.cities (
    id uuid NOT NULL,
    id_state uuid NOT NULL,
    name character varying(50) NOT NULL
);
    DROP TABLE public.cities;
       public         heap    postgres    false            �            1259    16387 	   countries    TABLE     a   CREATE TABLE public.countries (
    id uuid NOT NULL,
    name character varying(50) NOT NULL
);
    DROP TABLE public.countries;
       public         heap    postgres    false            �            1259    16390 	   locations    TABLE     �   CREATE TABLE public.locations (
    id uuid NOT NULL,
    id_country uuid NOT NULL,
    id_state uuid NOT NULL,
    id_city uuid NOT NULL,
    name character varying(50) NOT NULL
);
    DROP TABLE public.locations;
       public         heap    postgres    false            �            1259    16393    roles    TABLE     ]   CREATE TABLE public.roles (
    id uuid NOT NULL,
    name character varying(50) NOT NULL
);
    DROP TABLE public.roles;
       public         heap    postgres    false            �            1259    16463    sessions    TABLE     �   CREATE TABLE public.sessions (
    id uuid NOT NULL,
    id_user uuid NOT NULL,
    username character varying(50) NOT NULL,
    is_logged boolean NOT NULL
);
    DROP TABLE public.sessions;
       public         heap    postgres    false            �            1259    16396    states    TABLE     |   CREATE TABLE public.states (
    id uuid NOT NULL,
    id_country uuid NOT NULL,
    name character varying(50) NOT NULL
);
    DROP TABLE public.states;
       public         heap    postgres    false            �            1259    16399    users    TABLE     w  CREATE TABLE public.users (
    id uuid NOT NULL,
    id_role uuid NOT NULL,
    id_location uuid NOT NULL,
    username character varying(50) NOT NULL,
    password character varying(50) NOT NULL,
    firstname character varying(50) NOT NULL,
    lastname character varying(50) NOT NULL,
    email character varying(50) NOT NULL,
    phone character varying(50) NOT NULL
);
    DROP TABLE public.users;
       public         heap    postgres    false            8          0    16384    cities 
   TABLE DATA           4   COPY public.cities (id, id_state, name) FROM stdin;
    public          postgres    false    214   �$       9          0    16387 	   countries 
   TABLE DATA           -   COPY public.countries (id, name) FROM stdin;
    public          postgres    false    215   �%       :          0    16390 	   locations 
   TABLE DATA           L   COPY public.locations (id, id_country, id_state, id_city, name) FROM stdin;
    public          postgres    false    216   %&       ;          0    16393    roles 
   TABLE DATA           )   COPY public.roles (id, name) FROM stdin;
    public          postgres    false    217   $'       >          0    16463    sessions 
   TABLE DATA           D   COPY public.sessions (id, id_user, username, is_logged) FROM stdin;
    public          postgres    false    220   �'       <          0    16396    states 
   TABLE DATA           6   COPY public.states (id, id_country, name) FROM stdin;
    public          postgres    false    218   �'       =          0    16399    users 
   TABLE DATA           p   COPY public.users (id, id_role, id_location, username, password, firstname, lastname, email, phone) FROM stdin;
    public          postgres    false    219   b(       �           2606    16403    cities cities_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.cities
    ADD CONSTRAINT cities_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.cities DROP CONSTRAINT cities_pkey;
       public            postgres    false    214            �           2606    16405    countries countries_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.countries
    ADD CONSTRAINT countries_pkey PRIMARY KEY (id);
 B   ALTER TABLE ONLY public.countries DROP CONSTRAINT countries_pkey;
       public            postgres    false    215            �           2606    16407    locations locations_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.locations
    ADD CONSTRAINT locations_pkey PRIMARY KEY (id);
 B   ALTER TABLE ONLY public.locations DROP CONSTRAINT locations_pkey;
       public            postgres    false    216            �           2606    16409    roles roles_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.roles DROP CONSTRAINT roles_pkey;
       public            postgres    false    217            �           2606    16411    states states_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.states
    ADD CONSTRAINT states_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.states DROP CONSTRAINT states_pkey;
       public            postgres    false    218            �           2606    16413    users users_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.users DROP CONSTRAINT users_pkey;
       public            postgres    false    219            �           2606    16414    locations id_city    FK CONSTRAINT     {   ALTER TABLE ONLY public.locations
    ADD CONSTRAINT id_city FOREIGN KEY (id_city) REFERENCES public.cities(id) NOT VALID;
 ;   ALTER TABLE ONLY public.locations DROP CONSTRAINT id_city;
       public          postgres    false    214    3223    216            �           2606    16419    states id_country    FK CONSTRAINT     �   ALTER TABLE ONLY public.states
    ADD CONSTRAINT id_country FOREIGN KEY (id_country) REFERENCES public.countries(id) NOT VALID;
 ;   ALTER TABLE ONLY public.states DROP CONSTRAINT id_country;
       public          postgres    false    3225    215    218            �           2606    16424    locations id_country    FK CONSTRAINT     �   ALTER TABLE ONLY public.locations
    ADD CONSTRAINT id_country FOREIGN KEY (id_country) REFERENCES public.countries(id) NOT VALID;
 >   ALTER TABLE ONLY public.locations DROP CONSTRAINT id_country;
       public          postgres    false    3225    216    215            �           2606    16429    users id_location    FK CONSTRAINT     �   ALTER TABLE ONLY public.users
    ADD CONSTRAINT id_location FOREIGN KEY (id_location) REFERENCES public.locations(id) NOT VALID;
 ;   ALTER TABLE ONLY public.users DROP CONSTRAINT id_location;
       public          postgres    false    3227    219    216            �           2606    16434    users id_role    FK CONSTRAINT     v   ALTER TABLE ONLY public.users
    ADD CONSTRAINT id_role FOREIGN KEY (id_role) REFERENCES public.roles(id) NOT VALID;
 7   ALTER TABLE ONLY public.users DROP CONSTRAINT id_role;
       public          postgres    false    3229    219    217            �           2606    16439    cities id_state    FK CONSTRAINT     z   ALTER TABLE ONLY public.cities
    ADD CONSTRAINT id_state FOREIGN KEY (id_state) REFERENCES public.states(id) NOT VALID;
 9   ALTER TABLE ONLY public.cities DROP CONSTRAINT id_state;
       public          postgres    false    214    3231    218            �           2606    16444    locations id_state    FK CONSTRAINT     }   ALTER TABLE ONLY public.locations
    ADD CONSTRAINT id_state FOREIGN KEY (id_state) REFERENCES public.states(id) NOT VALID;
 <   ALTER TABLE ONLY public.locations DROP CONSTRAINT id_state;
       public          postgres    false    218    3231    216            �           2606    16466    sessions id_user    FK CONSTRAINT     o   ALTER TABLE ONLY public.sessions
    ADD CONSTRAINT id_user FOREIGN KEY (id_user) REFERENCES public.users(id);
 :   ALTER TABLE ONLY public.sessions DROP CONSTRAINT id_user;
       public          postgres    false    220    219    3233            8   �   x������0���)|�JScm��Eaa<z��iR[�>�� ���1?����Y~�&3�,2��.�LU�M�~za?�6�Y|���}�9�-8��1R�5��|hS���v>&��D^0@C,9��I��<$�����:QM+�X�gx����WP����fA��"���t��hR�zNff��fw]�d-�(���eNa�Iw�R�w�:��RO�`>7      9   �   x��б�  ���́��h�&F'j�JB!!b�_���ozިE�>H�+#Y-=�E:㴝;BDCI������C])?cf�=����C�k[[xspS���-LTG�p������ƒ�6��<\(ӧQ�p�pj�����N��Is,      :   �   x����N�0�3y
�@Q[��9N�p`�Ā����#퐶��"�����ğ���6��Xկ��,um�j���m�Rϧ�r-HS4�d����D<!l2'�{�Fq�-m\S�"ɿ�FySZ9d'�@?NI^r`�\�����!!�"F<j�����q X�i|]i�c�O�#�ƣ�y�[L��$yY�|+�'���v�B��	��%Ҥ�w�\>|�Of���/���dI#��kc���      ;   f   x��41L1�0O�5H34�5I53ҵ4�Lѵ0�023 C�Ĕ��<.K�J�8J�r2�3R��Qn̙��Y�O�Zβ��r��5�LI-�L�#N����\1z\\\ ��Bt      >      x������ � �      <   �   x���K� �ᱬ�� R
S;5���\ښ�D��L�z�s�'_�_��lC\ܥ�z4�;�n�U���}r�~��﨣8$�g����Nc\���<=<!h���\2�l��5-PuVO�Y�B=Tm1�J9��0�Bq���eC�����hc�=c��^�      =   p  x��ջn�  ���"ޏ-R�N:v��c"?*�m�~}�'�$2C�;�B�\����b)4�5��:����x	T�;�$��gM�N�X1��OC�/��~�aR<7��;=/nt��)�]�}�1CW(8U���ֈ���Zm;����D�(D�� W*Y��U�ƍ1���ы�8D��W*[���yH�!�4��ȁ��J5�ӻ�tkC&i"(��9Еj�u���)����D`",r�+U%��\Z�67��0.�FY	(��7j�t�)'(f�<��^w��M�S�q�ϣVG���2�l�[����ή�Tj�`�A@�ZY�
S��Ե@$��w�a��^;@8�g(�90[��e}t��H�o��, ���{     