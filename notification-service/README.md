# notification-service

Service information
---

SERVICE_NAME: notification-service  
PORT: 2004

How to run notification-service
---

1. Run `mvn clean install` to build your application
1. Start application with `java -jar target/notification-service-1.0-SNAPSHOT.jar server config.yml`
1. To check that your application is running enter url `http://localhost:2004`

Health Check
---

To see your applications health enter url `http://localhost:2004/healthcheck`

How to run notification-service in docker using dockerfile
---

1. Run `docker build -t notification-service .`
1. Run `docker run -p 2004:2004 --net=host -it notification-service`

How to run notification-service in docker using dockercompose
---

1. Run `docker-compose up`

