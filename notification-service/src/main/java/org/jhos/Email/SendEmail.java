package org.jhos.Email;

import org.jhos.core.EmailMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

public class SendEmail {

    private static final Logger logger = LoggerFactory.getLogger(SendEmail.class);

    public static void sendEmail(EmailMessage emailMessage) {

        final String username = "ramisohj2@gmail.com"; //Account for Google APP
        final String password = "civdxllpydqbxuxp";

        Properties prop = new Properties(); // properties for allow sending to another mail domains
        prop.put("mail.smtp.host", "smtp.gmail.com");
        prop.put("mail.smtp.port", "587");
        prop.put("mail.smtp.auth", "true");
        prop.put("mail.smtp.starttls.enable", "true");
        prop.put("mail.smtp.ssl.trust", "smtp.gmail.com");
        prop.put("mail.smtp.ssl.protocols", "TLSv1.2");

        Session session = Session.getInstance(prop,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(username));
            message.setRecipients(
                    Message.RecipientType.TO,
                    InternetAddress.parse(emailMessage.email)
            );
            message.setSubject(emailMessage.subject);
            message.setText(emailMessage.message);

            Transport.send(message);

            logger.info("MESSAGE TO: {} WAS SENT", emailMessage.email);

        } catch (MessagingException e) {
            logger.error("Exception: Error sending email ", e.getMessage());
            e.printStackTrace();
        }
    }

}