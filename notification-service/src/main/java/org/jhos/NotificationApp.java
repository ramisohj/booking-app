package org.jhos;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.dropwizard.core.Application;
import io.dropwizard.core.setup.Bootstrap;
import io.dropwizard.core.setup.Environment;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.jhos.Email.SendEmail;
import org.jhos.core.EmailMessage;
import org.jhos.health.ApplicationHealthCheck;
import org.jhos.resources.EmailResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;

public class NotificationApp extends Application<NotificationConf> {

    private static final Logger logger = LoggerFactory.getLogger(NotificationApp.class);
    private static final String topic = "sendEmail";

    public static void main(final String[] args) throws Exception {

        new NotificationApp().run(args);

        Properties props = new Properties();
        props.put("bootstrap.servers", "localhost:29092");
        props.put("group.id", "group1");
        props.put("enable.auto.commit", "true");
        props.put("auto.commit.interval.ms", "1000");
        props.put("session.timeout.ms", "30000");
        props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        KafkaConsumer<String, String> consumer = new KafkaConsumer<>(props);

        consumer.subscribe(Arrays.asList(topic));

        while (true) {
            ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(100));
            for (ConsumerRecord<String, String> record : records) {

                logger.info("topic: " + topic);
                logger.info("record: " + record.toString());

                ObjectMapper mapper = new ObjectMapper();
                EmailMessage emailMessage = mapper.readValue(record.value(), EmailMessage.class);
                SendEmail.sendEmail(emailMessage);
            }
        }
    }

    @Override
    public String getName() {
        return "notification-service";
    }

    @Override
    public void initialize(final Bootstrap<NotificationConf> bootstrap) {
        // TODO: application initialization
    }

    @Override
    public void run(final NotificationConf configuration,
                    final Environment environment) {
        environment
                .healthChecks()
                .register("application", new ApplicationHealthCheck());
        EmailResource er = new EmailResource();

        environment.jersey().register(er);
    }

}
