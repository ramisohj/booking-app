package org.jhos.core;

import com.fasterxml.jackson.annotation.JsonProperty;

public class EmailMessage {

    @JsonProperty("email")
    public String email;

    @JsonProperty("subject")
    public String subject;

    @JsonProperty("message")
    public String message;

}
