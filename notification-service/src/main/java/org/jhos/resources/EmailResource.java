package org.jhos.resources;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;


@Path("/email")
@Produces(MediaType.APPLICATION_JSON)
public class EmailResource {

    @GET
    @Path("/{text}")
    public Response getText(@PathParam("text") final String text) {
        return Response.ok(text).build();
    }
}
