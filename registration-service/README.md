# registration-service

Service information
---

SERVICE_NAME: registration-service  
PORT: 2000

How to run registration-service
---

1. Run `mvn clean install` to build your application
1. Start application with `java -jar target/registration-service-1.0-SNAPSHOT.jar server config.yml`
1. To check that your application is running enter url `http://localhost:2000`

Health Check
---

To see your applications health enter url `http://localhost:2000/healthcheck`

How to run registration-service in docker using dockerfile
---

1. Run `docker build -t  registration-service .`
1. Run `docker run -p 2000:2000 --net=host -it registration-service`

How to run registration-service in docker using dockercompose
---

1. Run `docker-compose up`
