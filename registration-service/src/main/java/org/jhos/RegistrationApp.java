package org.jhos;

import io.dropwizard.core.Application;
import io.dropwizard.core.setup.Bootstrap;
import io.dropwizard.core.setup.Environment;
import io.dropwizard.jdbi3.JdbiFactory;
import org.jdbi.v3.core.Jdbi;
import org.jhos.resources.CountryResource;
import org.jhos.resources.NotificationResource;
import org.jhos.resources.RoleResource;
import org.jhos.resources.UserResource;
import org.jhos.resources.AuthResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class RegistrationApp extends Application<RegistrationConf> {

    private static final Logger logger = LoggerFactory.getLogger(RegistrationApp.class);

    public static void main(final String[] args) throws Exception {
        new RegistrationApp().run(args);
    }

    @Override
    public String getName() {
        return "registration-service";
    }

    @Override
    public void initialize(final Bootstrap<RegistrationConf> bootstrap) {
        // TODO: application initialization
    }

    @Override
    public void run(RegistrationConf trueConfiguration, Environment environment) throws Exception {
        final JdbiFactory factory = new JdbiFactory();
        final Jdbi jdbi = factory.build(environment, trueConfiguration.getDataSourceFactory(), "postgresql");

        environment.jersey().register(new UserResource(jdbi));
        environment.jersey().register(new RoleResource(jdbi));
        environment.jersey().register(new AuthResource(jdbi));
        environment.jersey().register(new CountryResource(jdbi));
        environment.jersey().register(new NotificationResource());
    }
}
