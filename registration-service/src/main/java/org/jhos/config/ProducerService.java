package org.jhos.config;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;


public class ProducerService {

    private static final Logger logger = LoggerFactory.getLogger(ProducerService.class);

    public boolean sendMessageToTopic(String topic, String email, String subject, String message) {

        boolean wasSent = false;
        try {
            final Producer<String, String> producer;
            final Properties props = new Properties();
            props.put("bootstrap.servers", "localhost:29092");
            props.put("group.id", "group1");
            props.put("acks", "all");
            props.put("retries", 0);
            props.put("batch.size", 16384);
            props.put("linger.ms", 1);
            props.put("buffer.memory", 33554432);
            props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
            props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");

            String value = String.format("{\"email\": \"%s\", \"subject\": \"%s\",  \"message\": \"%s\"}", email, subject, message);

            producer = new KafkaProducer<>(props);
            producer.send(new ProducerRecord<>(topic, value));
            producer.close();
            wasSent =  true;
            logger.info("topic is: {}", topic);
            logger.info("value is: {}", value);

        } catch (Exception e) {
            logger.error("Exception: Error sending data to topic ", topic, e.getMessage());
            e.printStackTrace();
        }
        return wasSent;
    }
}
