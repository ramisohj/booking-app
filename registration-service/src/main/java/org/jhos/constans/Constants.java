package org.jhos.constans;

public class Constants {

    public static final String TOPIC = "sendEmail";

    // New user created:
    public static final String SUBJECT_USER_CREATED = "New user created on Booking-app";
    public static final String MESSAGE_USER_CREATED = "Thanks for use our App, Enjoy the new features.";

    // User account deleted
    public static final String SUBJECT_USER_DELETED = "Your user was deleted from Booking-app";
    public static final String MESSAGE_USER_DELETED = "Your user account was deleted from our App.";

    // Message for many login attempts
    public static final String SUBJECT_USER_LOGIN_FAIL = "Booking-app detected login failure";
    public static final String MESSAGE_USER_LOGIN_FAIL = "Seems you are trying to login, Are you sure you are trying to login now? ";

}
