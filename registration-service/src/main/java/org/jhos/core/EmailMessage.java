package org.jhos.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;


@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class EmailMessage {

    @NotNull
    @NotBlank
    @NotEmpty
    public String email;

    @NotNull
    @NotBlank
    @NotEmpty
    public String subject;

    @NotNull
    @NotBlank
    @NotEmpty
    public String message;

}
