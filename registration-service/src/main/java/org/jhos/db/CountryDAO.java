package org.jhos.db;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jhos.core.Country;

import java.util.List;
import java.util.UUID;


public interface CountryDAO {

    @SqlQuery("select * from countries where id = :id")
    @RegisterBeanMapper(Country.class)
    Country getCountry(@Bind("id") UUID id);

    @SqlQuery("select * from countries")
    @RegisterBeanMapper(Country.class)
    List<Country> getCountries();

}
