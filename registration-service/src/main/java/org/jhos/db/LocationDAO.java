package org.jhos.db;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jhos.core.Location;


public interface LocationDAO {

    @SqlQuery("select * from locations where name = 'Plaza Principal'")
    @RegisterBeanMapper(Location.class)
    Location getDefaultLocation();

}
