package org.jhos.db;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jhos.core.Role;


public interface RoleDAO {

    @SqlQuery("select * from roles where name = \'public\'")
    @RegisterBeanMapper(Role.class)
    Role getDefaultRole();

}
