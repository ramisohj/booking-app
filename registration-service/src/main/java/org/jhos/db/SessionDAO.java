package org.jhos.db;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jhos.core.Session;

import java.util.List;
import java.util.UUID;

public interface SessionDAO {

    @SqlQuery("select * from sessions")
    @RegisterBeanMapper(Session.class)
    List<Session> getSessions();

    @SqlQuery("select * from sessions where id = :id")
    @RegisterBeanMapper(Session.class)
    Session getSessionById(@Bind("id") UUID id);

    @SqlQuery("select * from sessions where id_user = :id_user")
    @RegisterBeanMapper(Session.class)
    Session getSessionByUserId(@Bind("id_user") UUID id);

    @SqlUpdate("insert into sessions(id, id_user, username, is_logged) values (:id, :id_user, :username, :is_logged)")
    void createSession(
            @Bind("id") UUID id,
            @Bind("id_user") UUID id_user,
            @Bind("username") String username,
            @Bind("is_logged") boolean is_logged);

    @SqlUpdate("delete from users where id = :id")
    void deleteUser(@Bind("id") UUID id);

    @SqlUpdate("delete from sessions where id = :id")
    void deleteSession(@Bind("id") UUID id);
}
