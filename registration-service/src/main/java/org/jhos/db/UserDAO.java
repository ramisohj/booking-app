package org.jhos.db;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jhos.core.User;

import java.util.List;
import java.util.UUID;


public interface UserDAO {

    String createUserQuery =
            "insert into users (id, id_role, id_location, username, password, firstname, lastname, email, phone)" +
                    "values (:id, :id_role, :id_location, :username, :password, :firstname, :lastname, :email, :phone)";

    @SqlUpdate(createUserQuery)
    void createUser(
            @Bind("id") UUID id,
            @Bind("id_role") UUID id_role,
            @Bind("id_location") UUID id_location,
            @Bind("username") String username,
            @Bind("password") String password,
            @Bind("firstname") String firstname,
            @Bind("lastname") String lastname,
            @Bind("email") String email,
            @Bind("phone") String phone);

    @SqlQuery("select * from users")
    @RegisterBeanMapper(User.class)
    List<User> getUsers();

    @SqlQuery("select * from users where id = :id")
    @RegisterBeanMapper(User.class)
    User getUser(@Bind("id") UUID id);

    @SqlQuery("select * from users where username = :username")
    @RegisterBeanMapper(User.class)
    User getUserByUsername(@Bind("username") String username);

    @SqlQuery("select * from users where email = :email")
    @RegisterBeanMapper(User.class)
    User getUserByEmail(@Bind("email") String email);

    @SqlUpdate("delete from users where id = :id")
    void deleteUser(@Bind("id") UUID id);
}

