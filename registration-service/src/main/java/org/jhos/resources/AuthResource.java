package org.jhos.resources;

import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.jdbi.v3.core.Jdbi;
import org.jhos.core.EmailMessage;
import org.jhos.core.Session;
import org.jhos.core.User;
import org.jhos.db.SessionDAO;
import org.jhos.exceptions.InvalidRequestException;
import org.jhos.exceptions.UserAuthenticationException;
import org.jhos.request.LoginRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.UUID;

import static org.jhos.constans.Constants.*;


@Path("/auth")
@Produces(MediaType.APPLICATION_JSON)
public class AuthResource {

    private static final Logger logger = LoggerFactory.getLogger(AuthResource.class);
    private final SessionDAO dao;
    private final UserResource userResource;
    private final NotificationResource notificationResource;
    private String lastUsername = "";
    private int countLoginAttempts = 0;

    public AuthResource(final Jdbi jdbi) {
        dao = jdbi.onDemand(SessionDAO.class);
        userResource = new UserResource(jdbi);
        notificationResource = new NotificationResource();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/logout/{id}")
    public Response logout(@PathParam("id") final String id) {

        try {
            Session session = dao.getSessionById(UUID.fromString(id));
             if (session==null) {
                 throw new InvalidRequestException("Wrong session object.");
             } else {
                 dao.deleteSession(UUID.fromString(id));
                 return Response.status(200).entity(session).build();
             }
        } catch ( Exception e) {
            return Response.status(500).entity(e.getMessage()).build();
        }
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/login")
    public Response login(LoginRequest loginRequest) {
        try {
            User user = userResource.getUserByUsername(loginRequest.getUsername());

            if (user != null) {
                if (user.getPassword().equals(loginRequest.getPassword())) {
                    dao.createSession(
                            UUID.fromString(UUID.randomUUID().toString()),
                            UUID.fromString(user.getId()),
                            user.getUsername(),
                            true);
                } else {
                    if (lastUsername.equals(user.getUsername())) {
                        countLoginAttempts++;
                        if (countLoginAttempts == 3){
                            EmailMessage emailMessage = new EmailMessage(user.getEmail(),
                                    SUBJECT_USER_LOGIN_FAIL,
                                    MESSAGE_USER_LOGIN_FAIL);
                            notificationResource.sendMessageToUser(TOPIC, emailMessage);
                            logger.info("User: {} has many login attempts", user.getUsername());
                        }
                    } else {
                        lastUsername = user.getUsername();
                    }
                    logger.info("User: {} with wrong password", user.getUsername());
                    throw new UserAuthenticationException("Invalid username or password. Try again.");
                }
            } else {
                throw new UserAuthenticationException("Invalid credentials for this user !!!");
            }

            return Response.ok(user).build();

        } catch (Exception e){
            logger.error(e.getMessage());
            return Response.status(500).entity(e.getMessage()).build();
        }
    }
}
