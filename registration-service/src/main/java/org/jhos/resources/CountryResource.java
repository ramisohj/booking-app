package org.jhos.resources;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.jdbi.v3.core.Jdbi;
import org.jhos.core.Country;
import org.jhos.db.CountryDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.UUID;


@Path("/countries")
@Produces(MediaType.APPLICATION_JSON)
public class CountryResource {

    private static final Logger logger = LoggerFactory.getLogger(CountryResource.class);
    private final CountryDAO dao;

    public CountryResource(final Jdbi jdbi) {
        dao = jdbi.onDemand(CountryDAO.class);
    }

    @GET
    public Response getCountries() {
        try {
            List<Country> ls = dao.getCountries();
            return Response.ok(ls).build();
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            return Response.status(500).entity(e.getMessage()).build();
        }
    }

    @GET
    @Path("/{id}")
    public Response getCountry(@PathParam("id") final String id) {
        try {
            Country country = dao.getCountry(UUID.fromString(id));
            return Response.ok(country).build();
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            return Response.status(500).entity(e.getMessage()).build();
        }
    }

}
