package org.jhos.resources;

import ch.qos.logback.core.encoder.EchoEncoder;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.jdbi.v3.core.Jdbi;
import org.jhos.core.Location;
import org.jhos.db.LocationDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Path("/location")
@Produces(MediaType.APPLICATION_JSON)
public class LocationResource {

    private static final Logger logger = LoggerFactory.getLogger(LocationResource.class);
    private final LocationDAO dao;

    public LocationResource(final Jdbi jdbi) {
        dao = jdbi.onDemand(LocationDAO.class);
    }

    public Location getDefaultLocation() {
        try {
            Location defaultLocation = dao.getDefaultLocation();
            return defaultLocation;
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            return null;
        }
    }

}