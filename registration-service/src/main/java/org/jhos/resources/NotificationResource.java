package org.jhos.resources;


import com.codahale.metrics.annotation.Timed;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.jhos.core.EmailMessage;
import org.jhos.request.ProducerRequest;
import org.jhos.config.ProducerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;


@Path("/notification")
@Produces(MediaType.APPLICATION_JSON)
public class NotificationResource {

    private static final Logger logger = LoggerFactory.getLogger(NotificationResource.class);


    @POST
    @Timed
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response sendMessageToTopic(ProducerRequest producerRequest) throws Exception {
        logger.info("Request received is: " + producerRequest );
        Map<String, String> response = new HashMap<>();
        ProducerService producerService = new ProducerService();
        boolean messageWasSent = producerService.sendMessageToTopic(
                producerRequest.getTopic(),
                producerRequest.getEmail(),
                producerRequest.getSubject(),
                producerRequest.getMessage());

        if (messageWasSent) {
            response.put("Message", "Message has been sent to Topic: " + producerRequest.getTopic());
        }
        else{
            response.put("Message", "Error in sending Message to Topic: " + producerRequest.getTopic());
        }

        return Response.ok(response).build();
    }

    public void sendMessageToUser(String topic, EmailMessage emailMessage) {
        try {
            ProducerService producerService = new ProducerService();

            boolean messageWasSent = producerService.sendMessageToTopic(
                    topic,
                    emailMessage.email,
                    emailMessage.subject,
                    emailMessage.message);

            if (messageWasSent) {
                logger.info("EMAIL SENT TO: {}", emailMessage.email);
            } else {
                logger.error("EMAIL WAS NOT SENT !!!");
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            e.printStackTrace();
        }
    }
}
