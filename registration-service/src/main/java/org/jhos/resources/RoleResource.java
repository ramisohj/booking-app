package org.jhos.resources;

import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import org.jdbi.v3.core.Jdbi;
import org.jhos.core.Role;
import org.jhos.core.User;
import org.jhos.db.RoleDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Path("/role")
@Produces(MediaType.APPLICATION_JSON)
public class RoleResource {

    private static final Logger logger = LoggerFactory.getLogger(RoleResource.class);
    private final RoleDAO dao;

    public RoleResource(final Jdbi jdbi) {
        dao = jdbi.onDemand(RoleDAO.class);
    }

    public Role getDefaultRole() {
        try {
            Role role = dao.getDefaultRole();
            return role;
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            return null;
        }
    }
}