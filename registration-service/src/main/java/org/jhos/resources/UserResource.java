package org.jhos.resources;

import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.jdbi.v3.core.Jdbi;
import org.jhos.core.EmailMessage;
import org.jhos.core.Location;
import org.jhos.core.Role;
import org.jhos.core.User;
import org.jhos.db.UserDAO;
import org.jhos.exceptions.DuplicateEmailException;
import org.jhos.exceptions.DuplicateUsernameException;
import org.jhos.exceptions.UserNotFoundException;
import org.jhos.request.CreateUserRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.UUID;

import static org.jhos.constans.Constants.*;


@Path("/users")
@Produces(MediaType.APPLICATION_JSON)
public class UserResource {

    private static final Logger logger = LoggerFactory.getLogger(UserResource.class);
    private final UserDAO dao;
    private final RoleResource roleResource;
    private final LocationResource locationResource;
    private final NotificationResource notificationResource;

    public UserResource(final Jdbi jdbi) {
        dao = jdbi.onDemand(UserDAO.class);
        roleResource = new RoleResource(jdbi);
        locationResource = new LocationResource(jdbi);
        notificationResource = new NotificationResource();
    }

    @GET
    public Response getUsers() {
        try {
            List<User> users = dao.getUsers();
            return Response.ok(users).build();
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            return Response.status(500).entity(e.getMessage()).build();
        }
    }

    @GET
    @Path("/{id}")
    public Response getUser(@PathParam("id") final String id) {
        try {
            User user = dao.getUser(UUID.fromString(id));
            return Response.ok(user).build();
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            return Response.status(500).entity(e.getMessage()).build();
        }
    }

    @DELETE
    @Path("/{id}")
    public Response deleteUser(@PathParam("id") final String id) {
        try {
            User user = getUser(UUID.fromString(id));

            if (user != null) {
                dao.deleteUser(UUID.fromString(id));

                EmailMessage emailMessage= new EmailMessage(user.getEmail(), SUBJECT_USER_DELETED, MESSAGE_USER_DELETED);
                notificationResource.sendMessageToUser(TOPIC,emailMessage);

                return Response.ok(user).build();
            } else {
                throw new UserNotFoundException("User doesn't exists !!!");
            }
        } catch ( Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            return Response.status(500).entity(e.getMessage()).build();
        }
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/create")
    public Response createUser(CreateUserRequest createUserRequest) {
        try {
            if (!isValidUserRequest(createUserRequest)) {
                throw new DuplicateUsernameException("Some fields in this request are invalid !!!");
            }
            User userByUsername = getUserByUsername(createUserRequest.getUsername());
            if (userByUsername != null) {
                throw new DuplicateUsernameException("Username user already exists");
            }
            User userByEmail = getUserByEmail(createUserRequest.getEmail());
            if (userByEmail != null) {
                throw new DuplicateEmailException("Email user already exists");
            }

            Role defaultRole = roleResource.getDefaultRole();
            Location defaultLocation = locationResource.getDefaultLocation();
            UUID newUserID = UUID.randomUUID();
            dao.createUser(
                    newUserID,
                    UUID.fromString(defaultRole.getId()),
                    UUID.fromString(defaultLocation.getId()),
                    createUserRequest.getUsername(),
                    createUserRequest.getPassword(),
                    createUserRequest.getFirstname(),
                    createUserRequest.getLastname(),
                    createUserRequest.getEmail(),
                    createUserRequest.getPhone());
            User newUser = getUser(newUserID);
            EmailMessage emailMessage= new EmailMessage(newUser.getEmail(), SUBJECT_USER_CREATED, MESSAGE_USER_CREATED);
            notificationResource.sendMessageToUser(TOPIC,emailMessage);
            return Response.status(201).entity(newUser).build();

        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            return Response.status(500).entity(e.getMessage()).build();
        }
    }

    public User getUser(UUID id) {
        try {
            User user = dao.getUser(id);
            return user;
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            return null;
        }
    }

    public User getUserByUsername(@PathParam("username") final String username) {
        try {
            User user = dao.getUserByUsername(username);
            return user;
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            return null;
        }
    }

    public User getUserByEmail(@PathParam("email") final String email) {
        try {
            User user = dao.getUserByEmail(email);
            return user;
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            return null;
        }
    }

    private boolean isValidUserRequest(CreateUserRequest createUserRequest) {
        if (createUserRequest == null) {
            return false;
        }
        if (createUserRequest.getUsername().trim().isEmpty() ||
        createUserRequest.getPassword().trim().isEmpty() ||
        createUserRequest.getFirstname().trim().isEmpty() ||
        createUserRequest.getLastname().trim().isEmpty() ||
        createUserRequest.getEmail().trim().isEmpty()) {
            return false;
        }
        return true;
    }
}