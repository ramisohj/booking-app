# search-service

Service information
---

SERVICE_NAME: search-service  
PORT: 2002

How to run search-service
---

1. Run `mvn clean install` to build your application
1. Start application with `java -jar target/search-service-1.0-SNAPSHOT.jar server config.yml`
1. To check that your application is running enter url `http://localhost:2002`

Health Check
---

To see your applications health enter url `http://localhost:2002/healthcheck`

How to run search-service in docker using dockerfile
---

1. Run `docker build -t  search-service .`
1. Run `docker run -p 2002:2002 --net=host -it search-service`

How to run search-service in docker using dockercompose
---

1. Run `docker-compose up`