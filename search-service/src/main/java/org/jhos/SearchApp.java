package org.jhos;

import io.dropwizard.core.Application;
import io.dropwizard.core.setup.Bootstrap;
import io.dropwizard.core.setup.Environment;
import org.jhos.health.ApplicationHealthCheck;

public class SearchApp extends Application<SearchConf> {

    public static void main(final String[] args) throws Exception {
        new SearchApp().run(args);
    }

    @Override
    public String getName() {
        return "true";
    }

    @Override
    public void initialize(final Bootstrap<SearchConf> bootstrap) {
        // TODO: application initialization
    }

    @Override
    public void run(final SearchConf configuration,
                    final Environment environment) {
        environment
                .healthChecks()
                .register("application", new ApplicationHealthCheck());
    }

}
